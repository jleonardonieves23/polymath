package com.polymath.app.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "tareas")
public class Tarea  implements Serializable { 
	
	private static final long serialVersionUID = 1L; 	

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column(name = "tarea_id")
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@Column(length = 500)
	@NotEmpty
	private String description;
	
	@ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "estatu_id")
	private Estatus estatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	} 

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Estatus getEstatus() {
		return estatus;
	}

	public void setEstatus(Estatus estatus) {
		this.estatus = estatus;
	}

}
