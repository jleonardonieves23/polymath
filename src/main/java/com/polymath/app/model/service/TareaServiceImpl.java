package com.polymath.app.model.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.polymath.app.dao.ITareaDao;
import com.polymath.app.model.entity.Tarea;
import com.polymath.app.model.entity.Usuario;

@Service
public class TareaServiceImpl implements ITareaService {

	private ITareaDao iTareaDao;

	@Autowired
	public TareaServiceImpl(ITareaDao iTareaDao) {
		this.iTareaDao = iTareaDao;
	}

	@Override
	public void createTarea(Tarea tarea) {
		iTareaDao.save(tarea);
	}

	@Override
	public void deleteTarea(Long id) {
		iTareaDao.deleteById(id); 
	}

	@Override
	public Tarea getByIdTareas(Long id) {
		return iTareaDao.findById(id).orElse(null);
	}

	@Override
	public Page<Tarea> getAllTareas(Pageable pageable,Usuario usuario) {
		return iTareaDao.findByUsuario(usuario, pageable);
	}

	@Override
	public Page<Tarea> getAllTareas(Pageable pageable, Usuario usuario, String description) {
		return iTareaDao.findByUsuarioAndDescriptionLike(usuario, description, pageable);
	}

}
