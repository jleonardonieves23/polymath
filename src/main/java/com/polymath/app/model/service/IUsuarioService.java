package com.polymath.app.model.service;

import com.polymath.app.model.entity.Usuario;

public interface IUsuarioService {

	void createUsuario(Usuario usuario);
	
	Usuario getByUsername(String username);
	
}
