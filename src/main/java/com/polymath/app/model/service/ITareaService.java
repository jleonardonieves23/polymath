package com.polymath.app.model.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.polymath.app.model.entity.Tarea;
import com.polymath.app.model.entity.Usuario;

public interface ITareaService {

	Page<Tarea> getAllTareas(Pageable pageable, Usuario usuario);
	
	Page<Tarea> getAllTareas(Pageable pageable, Usuario usuario, String description);
	
	Tarea getByIdTareas(Long Id);
	
	void createTarea(Tarea tarea);
	
	void deleteTarea(Long id);
}
