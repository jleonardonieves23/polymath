package com.polymath.app.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.polymath.app.dao.IRoleDao;
import com.polymath.app.dao.IUsuarioDao;
import com.polymath.app.model.entity.Role;
import com.polymath.app.model.entity.Usuario;

@Service
public class UsuarioService implements IUsuarioService {
	
	private IUsuarioDao iUsuarioDao; 
	
	private IRoleDao iRoleDao;

	@Autowired
	public UsuarioService(IUsuarioDao iUsuarioDao, IRoleDao iRoleDao) {
		this.iUsuarioDao = iUsuarioDao;
		this.iRoleDao = iRoleDao;
	}

	@Override
	public void createUsuario(Usuario usuario) {
		iUsuarioDao.save(usuario);
		Usuario u = iUsuarioDao.findByUsername(usuario.getUsername());
		Role role = new Role(); 
		role.setAuthority("ROLE_ADMIN");
		role.setUser_id(u.getId());
		iRoleDao.save(role);
	}

	@Override
	public Usuario getByUsername(String username) { 
		return iUsuarioDao.findByUsername(username);
	}

}
