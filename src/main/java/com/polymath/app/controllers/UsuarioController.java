package com.polymath.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.polymath.app.model.entity.Usuario;
import com.polymath.app.model.service.IUsuarioService;

@Controller
public class UsuarioController {

	@Autowired
	private IUsuarioService iUsuarioService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String crearUsuario(Map<String, Object> model) {
		Usuario usuario = new Usuario();
		model.put("titulo", "Registro de Nuevo Usuario");
		model.put("usuario", usuario);
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String guardarUsuario(@Valid Usuario usuario, BindingResult result) {

		if (result.hasErrors()) {
			return "register";
		}
		
		usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));		
		
		iUsuarioService.createUsuario(usuario);
		return "redirect:login";
	}

}
