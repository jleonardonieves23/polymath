package com.polymath.app.controllers;

import java.security.Principal;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.polymath.app.model.entity.Tarea;
import com.polymath.app.model.entity.Usuario;
import com.polymath.app.model.service.ITareaService;
import com.polymath.app.model.service.IUsuarioService;
import com.polymath.app.utilPaginator.PageRender;

@Controller
@SessionAttributes("tarea")
public class TareaController {

	@Autowired
	private ITareaService iTareaService;
	
	@Autowired
	private IUsuarioService iUsuarioService;

	@RequestMapping(value = {"/listar","/"}, method = RequestMethod.GET)
	public String listar(@RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam(name = "description", required = false) String description,
			Model model, Principal principal, Authentication authentication) {
		
		if(authentication == null) {
			model.addAttribute("error", "Debe autenticarse");
			return "redirect:login";
		}
		
		String nameUser = principal.getName();
		Usuario usuario = iUsuarioService.getByUsername(nameUser);
		
		Pageable pageRequest = PageRequest.of(page, 4);
		Page<Tarea> tarea = ( description == null || description.equals("")) ? iTareaService.getAllTareas(pageRequest, usuario) : iTareaService.getAllTareas(pageRequest, usuario, description);
		PageRender<Tarea> pageRender = new PageRender<>("/listar", tarea);
		
		model.addAttribute("titulo", "Listar Tareas ");
		model.addAttribute("tarea", tarea);
		model.addAttribute("page", pageRender);
		return "listar";
	}

	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String crear(Map<String, Object> model, Authentication authentication, Principal principal) {
		if(authentication == null) {
			model.put("error", "Debe autenticarse");
			return "redirect:login";
		}
		
		String nameUser = principal.getName();
		Usuario usuario = iUsuarioService.getByUsername(nameUser);
		
		Tarea tarea = new Tarea();
		tarea.setUsuario(usuario);
		model.put("tarea", tarea);
		model.put("titulo", "Crea Nueva Tareas");
		return "form";
	}
	
	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET)
	public String editar(@PathVariable(value = "id") Long id,  Map<String, Object> model, 
			RedirectAttributes flash, Authentication authentication) {	
		if(authentication == null) {
			flash.addFlashAttribute("error", "Debe autenticarse");
			return "redirect:login";
		}	
		Tarea tarea = null;		
		if(id > 0) {
			tarea = iTareaService.getByIdTareas(id);
			if (tarea == null) {
				flash.addFlashAttribute("error", "El id de la tarea no existe en base de dato");
				return "redirect:listar";
			}
		} else {
			flash.addFlashAttribute("error", "El id de tarea no puede ser 0");
			return "redirect:listar";
		}
		model.put("tarea", tarea);
		model.put("titulo", "Editar Tareas");
		return "form";
	}

	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String guardar(@Valid Tarea tarea, BindingResult result, Model model, RedirectAttributes flash, 
			SessionStatus status, Authentication authentication) { 		
		if(authentication == null) {
			model.addAttribute("error", "Debe autenticarse");
			return "redirect:login";
		}
		if(result.hasErrors()) {
			model.addAttribute("titulo", "Crea Nueva Tareas");
			return "form";
		}		
		
		String msj = (tarea.getId() != null) ? "Tarea modificada con éxito" : "Tarea creada con éxito";
		iTareaService.createTarea(tarea);
		status.setComplete();
		flash.addFlashAttribute("success", msj);
		return "redirect:listar";
	}
	
	@RequestMapping(value = "/eliminar/{id}" )
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Authentication authentication) {
		if(authentication == null) {
			flash.addFlashAttribute("error", "Debe autenticarse");
			return "redirect:login";
		}
		if(id > 0) { 
			Tarea tarea = iTareaService.getByIdTareas(id);
			if(tarea != null ) {
				iTareaService.deleteTarea(id);
				flash.addFlashAttribute("success", "Tarea eliminada con éxito");
			}
		}		
		return "redirect:/listar";
	}
}
