package com.polymath.app.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.polymath.app.model.entity.Role;

public interface IRoleDao extends PagingAndSortingRepository<Role, Long>{

}
