package com.polymath.app.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.polymath.app.model.entity.Estatus;

public interface IEstatusDao extends PagingAndSortingRepository<Estatus, Long> {

}
