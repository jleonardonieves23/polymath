package com.polymath.app.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.polymath.app.model.entity.Tarea;
import com.polymath.app.model.entity.Usuario;

public interface ITareaDao  extends PagingAndSortingRepository<Tarea, Long> {

	Page<Tarea> findByUsuario(Usuario usuario, Pageable pageable);

	@Query("SELECT t FROM Tarea t WHERE t.usuario = ?1 AND t.description like %?2%")
	Page<Tarea> findByUsuarioAndDescriptionLike(Usuario usuario, String description, Pageable pageable);
	
	
}
