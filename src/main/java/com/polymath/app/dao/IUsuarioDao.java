package com.polymath.app.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.polymath.app.model.entity.Usuario;

public interface IUsuarioDao extends PagingAndSortingRepository<Usuario, Long>{

	Usuario findByUsername(String username);

}
