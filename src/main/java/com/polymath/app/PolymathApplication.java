package com.polymath.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PolymathApplication {

	public static void main(String[] args) {
		SpringApplication.run(PolymathApplication.class, args);
	}

}
